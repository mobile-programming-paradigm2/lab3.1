import 'dart:io';

void main(List<String> args) {
  bool end = false;

  while (!end) {
    print("---------------------------------------");
    print("1.Coffee");
    print("2.Tea");
    print("3.Milk, Cocoa and Caramel");
    print("4.Protein shake");
    print("5.Soda water and others");
    print("---------------------------------------");

    print("Select Item");
    int? select = int.parse(stdin.readLineSync()!);

    switch (select) {
      case 1:
        {
          print("Coffee");
          print("45 Baht");
        }

        break;
      case 2:
        {
          print("Tea");
          print("50 Baht");
        }

        break;
      case 3:
        {
          print("Milk, Cocoa and Caramel");
          print("45 Baht");
        }

        break;
      case 4:
        {
          print("Protein shake");
          print("55 Baht");
        }

        break;
      case 5:
        {
          print("Soda water and others");
          print("40 Baht");
        }

        break;
    }
    print("---------------------------------------");
    print("Sweetness");
    print("---------------------------------------");
    print("1.No sugar added");
    print("2.Little sweet");
    print("3.Sweet fit");
    print("4.So sweet");
    print("5.Sweet 3 worlds");
    print("---------------------------------------");

    print("Select Item");
    int? sweet = int.parse(stdin.readLineSync()!);

    switch (sweet) {
      case 1:
        {
          print("No sugar added");
        }

        break;
      case 2:
        {
          print("Little sweet");
        }

        break;
      case 3:
        {
          print("Sweet fit");
        }

        break;
      case 4:
        {
          print("So sweet");
        }

        break;
      case 5:
        {
          print("Sweet 3 worlds");
        }

        break;
    }
    print("---------------------------------------");
    print("Choose to pay");
    print("---------------------------------------");
    print("1.Cash");
    print("2.Pay via TrueMoney Wallet");
    print("3.Scan QR to pay");
    print("4.Use taobin credit");
    print("5.Use coupons/view accumulated ");
    print("6.Pay via Shopee Pay ");
    print("---------------------------------------");

    print("Select Item");
    int? pay = int.parse(stdin.readLineSync()!);
    switch (pay) {
      case 1:
        {
          print("Cash");
        }

        break;
      case 2:
        {
          print("Pay via TrueMoney Wallet");
        }

        break;
      case 3:
        {
          print("Scan QR to pay");
        }

        break;
      case 4:
        {
          print("Use taobin credit");
        }

        break;
      case 5:
        {
          print("Use coupons/view accumulated");
        }

        break;
      case 6:
        {
          print("Pay via Shopee Pay");
        }

        break;
    }
    print("---------------------------------------");
    print("Collect Tortoiseshell");
    print("To get the right to buy 11 get 1 free");
    print("---------------------------------------");
    print("1.Enter the tone number of the mobile phone.");
    print("2.Do not accept");
    print("---------------------------------------");

    print("Select Item");
    int? accumulate = int.parse(stdin.readLineSync()!);
    switch (accumulate) {
      case 1:
        {
          print("Please enter a phone number");
          int? x = int.parse(stdin.readLineSync()!);

          print("your reward points : 1");
        }

        break;
      case 2:
        {
          print("Do not accept");
        }

        break;
    }
    print("---------------------------------------");
    print("Press to select that you want to receive. and the glass lid or not");
    print("---------------------------------------");
    print("1.Bulb");
    print("2.Glass lid");
    print("3.Bulb and Glass lid");
    print("---------------------------------------");

    print("Select Item");
    int? receive = int.parse(stdin.readLineSync()!);
    switch (receive) {
      case 1:
        {
          print("Bulb");
        }

        break;
      case 2:
        {
          print("Glass lid");
        }

        break;
      case 3:
        {
          print("Bulb and Glass lid");
          end = true;
        }

        break;
    }
  }
}
